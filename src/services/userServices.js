/* eslint-disable no-undef */
/* eslint-disable no-useless-catch */
const bcrypt = require('bcrypt');
const User = require('../models/User');
const ROUND = '$2b$10$IfCuNZixdJa6EsW2MaVM0u';

const getUserService = async (id) => {
  if (!id) throw Error('Data is invalid!');

  const user = await User.findById(id);

  if (!user) throw Error('User not found!');
  return user;
};

const deleteUserService = async (id) => {
  try {
    if (!id) throw Error('Data is invalid!');
    const user = await User.findById(id);
    if (!user) throw Error('User not found!');

    await User.findOneAndDelete(user);
  } catch (err) {
    throw err;
  }
};

const changePasswordService = async (id, oldPassword, newPassword) => {
  try {
    if (!id || !oldPassword || !newPassword) throw Error('Data is invalid!');
    if (oldPassword === newPassword)
      throw Error('New password and old password must not the same!');

    const user = await User.findOne({
      $and: [
        { _id: id },
        { hash_password: bcrypt.hashSync(oldPassword, ROUND) },
      ],
    });
    if (!user) throw Error('User not found!');

    await User.findOneAndUpdate(user, {
      hash_password: bcrypt.hashSync(newPassword, ROUND),
    });
  } catch (err) {
    throw err;
  }
};

module.exports = { getUserService, deleteUserService, changePasswordService };
