/* eslint-disable no-useless-catch */
/* eslint-disable no-undef */
const Truck = require('../models/Truck');
const moment = require('moment');

const getTruckService = async (truckId, userId) => {
  if (!truckId || !userId) throw Error('Data is invalid!');

  const truck = await Truck.findById(truckId);
  if (!truck) throw Error('Truck not found!');
  if (truck.created_by !== userId) throw Error('User do not has permission!');

  return truck;
};

const assignTruckService = async (truckId, userId) => {
  try {
    if (!truckId || !userId) throw Error('Data is invalid!');

    const truck = await Truck.findById(truckId);
    if (!truck) throw Error('Truck not found!');
    if (truck.created_by !== userId) throw Error('User do not has permission!');

    await Truck.findOneAndUpdate(truck.toObject(), { assigned_to: userId });
  } catch (err) {
    throw err;
  }
};

const updateTruckService = async (truckId, userId, type) => {
  try {
    if (!truckId || !userId) throw Error('Data is invalid!');
    const truck = await Truck.findById(truckId);
    if (!truck) throw Error('Truck not found!');
    if (truck.created_by !== userId) throw Error('User do not has permission!');

    await Truck.findOneAndUpdate(truck.toObject(), { type });
  } catch (err) {
    throw err;
  }
};

const deleteTruckService = async (id) => {
  try {
    if (!id) throw Error('Truck not found!');
    await Truck.findOneAndDelete({ _id: id });
  } catch (err) {
    throw err;
  }
};

const getTruckListService = async (userId) => {
  try {
    if (!userId) throw Error('User is invalid!');
    const trucks = await Truck.find({ created_by: userId });
    return trucks;
  } catch (err) {
    throw err;
  }
};
const addTruckService = async (type, userId) => {
  try {
    if (!type) throw Error('Type is invalid!');
    const truck = new Truck({
      created_by: userId,
      assigned_to: '',
      type,
      status: 'OL',
      created_date: moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss'),
    });
    await truck.save();
  } catch (err) {
    throw err;
  }
};

module.exports = {
  getTruckService,
  assignTruckService,
  updateTruckService,
  deleteTruckService,
  getTruckListService,
  addTruckService,
};
