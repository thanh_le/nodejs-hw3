/* eslint-disable no-useless-catch */
/* eslint-disable no-undef */
const bcrypt = require('bcrypt');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const ROUND = '$2b$10$IfCuNZixdJa6EsW2MaVM0u';
const moment = require('moment');

const loginService = async (email, password) => {
  if (!email || !password) throw Error('Data is invalid!');

  const user = await User.findOne({
    $and: [
      { email: email },
      { hash_password: bcrypt.hashSync(password, ROUND) },
    ],
  });

  if (!user) throw Error('Email or password is incorrect!');

  const token = jwt.sign(
    {
      _id: user._id,
      email: user.email,
      role: user.role,
    },
    process.env.TOKEN_KEY,
    {
      expiresIn: '48h',
    }
  );
  return token;
};

const registerService = async (email, role, password) => {
  try {
    if (!email || !role || !password) throw Error('Data is invalid!');

    const user = await User.findOne({ email: email });
    if (user) throw Error('Email already exsit!');

    const newUser = new User({
      email,
      role,
      created_date: moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss'),
      hash_password: bcrypt.hashSync(password, ROUND),
    });

    await newUser.save();
    return;
  } catch (err) {
    throw err;
  }
};

const resetPasswordService = async (email, oldPassword, newPassword) => {
  try {
    if (!email || !oldPassword || !newPassword) throw Error('Data is invalid!');
    if (oldPassword === newPassword)
      throw Error('New password and old password must not the same!');

    const user = await User.findOne({
      $and: [
        { email: email },
        { hash_password: bcrypt.hashSync(oldPassword, ROUND) },
      ],
    });
    if (!user) throw Error('User not found!');

    await User.findOneAndUpdate(user.toObject(), {
      hash_password: bcrypt.hashSync(newPassword, ROUND),
    });
  } catch (err) {
    throw err;
  }
};

module.exports = { loginService, registerService, resetPasswordService };
