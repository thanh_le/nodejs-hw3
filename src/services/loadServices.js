/* eslint-disable no-useless-catch */
/* eslint-disable no-undef */
const Load = require('../models/Load');
const moment = require('moment');

const getShippingInfor = async () => {};
const getLoadService = async (loadId, userId) => {
  if (!loadId || !userId) throw Error('Data is invalid!');

  const load = await Load.findById(loadId);
  if (!load) throw Error('Load not found!');
  if (load.created_by !== userId && load.assigned_to !== userId)
    throw Error('User do not has permission!');

  return load;
};

const postLoadService = async (loadId, userId) => {
  try {
    if (!loadId || !userId) throw Error('Data is invalid!');

    const load = await Load.findById(loadId);
    if (!load) throw Error('Load not found!');
    if (load.created_by !== userId) throw Error('User do not has permission!');
  } catch (err) {
    throw err;
  }
};

const updateLoadService = async (data, loadId, userId) => {
  try {
    const { name, payload, pickup_address, delivery_address, dimensions } =
      data;
    if (!data) throw Error('Data is invalid!');
    const load = await Load.findById(loadId);
    if (!load) throw Error('Load not found!');
    if (load.created_by !== userId) throw Error('User do not has permission!');

    const newData = {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    };

    await Load.findByIdAndUpdate(loadId, newData);
  } catch (err) {
    throw err;
  }
};
const deleteLoadService = async (loadId, userId) => {
  try {
    if (!loadId || !userId) throw Error('Data is invalid!');
    const load = await Load.findById(loadId);
    console.log(userId);
    if (!load) throw Error('Load not found!');
    if (load.created_by !== userId) throw Error('User do not has permission!');

    await Load.findOneAndDelete(load.toObject());
  } catch (err) {
    throw err;
  }
};

const iterateNextState = async (loadId, userId) => {
  try {
    if (!loadId || !userId) throw Error('Data is invalid!');

    const load = await Load.findById(loadId);
    if (!load) throw Error('Load not found!');
    if (load.created_by !== userId) throw Error('User do not has permission!');

    await Load.findOneAndUpdate(load.toObject(), { assigned_to: userId });
  } catch (err) {
    throw err;
  }
};

const getActiveLoadList = async () => {};

const getLoadListService = async (userId) => {
  try {
    if (!userId) throw Error('User is invalid!');
    const loads = await Load.find({ created_by: userId });
    return loads;
  } catch (err) {
    throw err;
  }
};

const addLoadService = async (data, userId) => {
  try {
    const { name, payload, pickup_address, delivery_address, dimensions } =
      data;
    if (!data) throw Error('Data is invalid!');
    const load = new Load({
      created_by: userId,
      assigned_to: '',
      status: '',
      state: '',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      logs: [],
      created_date: moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss'),
    });
    await load.save();
  } catch (err) {
    throw err;
  }
};

module.exports = {
  getShippingInfor,
  getLoadService,
  postLoadService,
  updateLoadService,
  deleteLoadService,
  iterateNextState,
  getActiveLoadList,
  getLoadListService,
  addLoadService,
};
