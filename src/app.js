const express = require('express');
const app = express();
const cors = require('cors');
const router = require('./routes/index');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 8080;
const dbConnection = require('./db/connection');

app.use(express.json());
app.use(cors());
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
router(app);

dbConnection();
app.listen(PORT, () => {
  console.log(`App is listening at port ${PORT}`);
});
