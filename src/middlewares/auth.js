/* eslint-disable no-undef */
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  const token =
    req.body.token ||
    req.headers.jwt_token ||
    req.headers.authorization ||
    req.headers['x-access-token'];

  if (!token) {
    return res.status(401).send('A token is required for authentication');
  }
  jwt.verify(token, process.env.TOKEN_KEY, (err, user) => {
    if (err) return res.status(401).send('Invalid Token');
    req.user = user;
    next();
  });
};

const verifyDriver = (req, res, next) => {
  const user = req.user;

  if (!user) {
    return res.status(401).send('User is in valid');
  }
  if (user.role !== 'DRIVER')
    return res.status(400).send('Only driver is allowed!');
  next();
};

const verifyShipper = (req, res, next) => {
  const user = req.user;

  if (!user) {
    return res.status(401).send('User is in valid');
  }
  if (user.role !== 'SHIPPER')
    return res.status(400).send('Only shipper is allowed!');
  next();
};
module.exports = { verifyToken, verifyDriver, verifyShipper };
