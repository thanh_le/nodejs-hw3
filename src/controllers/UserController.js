/* eslint-disable no-undef */
const {
  getUserService,
  deleteUserService,
  changePasswordService,
} = require('../services/userServices');

class UserController {
  async getUser(req, res) {
    try {
      const _id = req.user._id;
      const user = await getUserService(_id);

      return res.status(200).json({
        user: {
          _id: user._id,
          email: user.email,
          role: user.role,
          created_date: user.created_date,
        },
      });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }

  async deleteUser(req, res) {
    try {
      const _id = req.user._id;
      await deleteUserService(_id);
      return res.status(200).json({ message: 'Success' });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }

  async changePassword(req, res) {
    try {
      const _id = req.user._id;
      const { oldPassword, newPassword } = req.body;
      await changePasswordService(_id, oldPassword, newPassword);
      return res.status(200).json({ message: 'Success' });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }
}

module.exports = new UserController();
