/* eslint-disable no-undef */
const {
  getTruckService,
  assignTruckService,
  updateTruckService,
  deleteTruckService,
  getTruckListService,
  addTruckService,
} = require('../services/truckServices');

class AuthController {
  async getTruck(req, res) {
    try {
      const truckId = req.params.id;
      const userId = req.user._id;
      const truck = await getTruckService(truckId, userId);
      res.status(200).json({ truck });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async assignTruck(req, res) {
    try {
      const truckId = req.params.id;
      const userId = req.user._id;
      await assignTruckService(truckId, userId);
      res.status(200).json({ message: 'Success' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async updateTruck(req, res) {
    try {
      const truckId = req.params.id;
      const userId = req.user._id;
      const type = req.body.type;
      await updateTruckService(truckId, userId, type);
      res.status(200).json({ message: 'Success' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async deleteTruck(req, res) {
    try {
      const id = req.params.id;
      await deleteTruckService(id);
      res.status(200).json({ message: 'Success' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async getTruckList(req, res) {
    try {
      const userId = req.user._id;

      const trucks = await getTruckListService(userId);
      res.status(200).json({ trucks });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }

  async addTruck(req, res) {
    try {
      const userId = req.user._id;
      console.log(userId);
      const type = req.body.type;
      await addTruckService(type, userId);
      res.status(200).json({ message: 'Success' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
}

module.exports = new AuthController();
