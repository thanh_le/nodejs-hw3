/* eslint-disable no-undef */
const {
  // getShippingInfor,
  getLoadService,
  postLoadService,
  updateLoadService,
  deleteLoadService,
  // iterateNextState,
  // getActiveLoadList,
  getLoadListService,
  addLoadService,
} = require('../services/loadServices');

class LoadController {
  async getShippingInfor(req, res) {
    res.send('get shipping');
  }
  async getLoad(req, res) {
    try {
      const loadId = req.params.id;
      const userId = req.user._id;
      const load = await getLoadService(loadId, userId);
      res.status(200).json({ load });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async postLoad(req, res) {
    try {
      const loadId = req.params.id;
      const userId = req.user._id;
      await postLoadService(loadId, userId);
      // send to socket
      res.status(200).json({
        message: 'Load posted successfully',
        driver_found: false,
      });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async updateLoad(req, res) {
    try {
      const data = req.body;
      const loadId = req.params.id;
      const userId = req.user._id;
      await updateLoadService(data, loadId, userId);
      res.status(200).json({ message: 'Load details changed successfully' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }

  async deleteLoad(req, res) {
    try {
      const loadId = req.params.id;
      const userId = req.user._id;
      await deleteLoadService(loadId, userId);
      res.status(200).json({ message: 'Load deleted successfully' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
  async iterateNextState(req, res) {
    res.send('iterate');
  }
  async getActiveLoadList(req, res) {
    res.send('get active load');
  }
  async getLoadList(req, res) {
    try {
      const userId = req.user._id;
      const loads = await getLoadListService(userId);
      res.status(200).json({ loads });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }

  async addLoad(req, res) {
    try {
      const data = req.body;
      const userId = req.user._id;
      await addLoadService(data, userId);
      res.status(200).json({ message: 'Load created successfully' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
}

module.exports = new LoadController();
