/* eslint-disable no-undef */
const {
  loginService,
  registerService,
  resetPasswordService,
} = require('../services/authServices');

class AuthController {
  async login(req, res) {
    try {
      const { email, password } = req.body;
      const token = await loginService(email, password);
      return res.status(200).json({ message: 'Success', jwt_token: token });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }

  async register(req, res) {
    try {
      const { email, role, password } = req.body;
      await registerService(email, role, password);

      return res.status(200).json({ message: 'Success' });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }

  async resetPassword(req, res) {
    try {
      const { email, oldPassword, newPassword } = req.body;
      await resetPasswordService(email, oldPassword, newPassword);

      return res.status(200).json({ message: 'Success' });
    } catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }
}

module.exports = new AuthController();
