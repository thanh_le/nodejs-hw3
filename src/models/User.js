/* eslint-disable no-undef */
const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema(
  {
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      require: true,
      auto: true,
    },
    email: String,
    role: String,
    created_date: String,
    hash_password: String,
  },
  { collection: 'User' }
);

module.exports = mongoose.model('User', schema);
