/* eslint-disable no-undef */
const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema(
  {
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      require: true,
      auto: true,
    },
    created_by: {
      type: String,
      require: true,
    },
    assigned_to: {
      type: String,
      require: true,
    },
    type: String,
    status: String,
    created_date: String,
  },
  { collection: 'Truck' }
);

module.exports = mongoose.model('Truck', schema);
