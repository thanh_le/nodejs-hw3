/* eslint-disable no-undef */
const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema(
  {
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      require: true,
      auto: true,
    },
    created_by: {
      type: String,
      require: true,
    },
    assigned_to: {
      type: String,
      require: true,
    },
    status: String,
    state: String,
    name: {
      type: String,
      require: true,
    },
    payload: {
      type: Number,
      require: true,
    },
    pickup_address: {
      type: String,
      require: true,
    },
    delivery_address: {
      type: String,
      require: true,
    },
    dimensions: {
      width: {
        type: Number,
        require: true,
      },
      length: {
        type: Number,
        require: true,
      },
      height: {
        type: Number,
        require: true,
      },
    },
    logs: [{ message: String, time: String }],
    created_date: {
      type: String,
      require: true,
    },
  },
  { collection: 'Load' }
);

module.exports = mongoose.model('Load', schema);
