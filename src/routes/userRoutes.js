/* eslint-disable no-undef */
const express = require('express');
const router = new express.Router();
const userController = require('../controllers/UserController');

router.patch('/password', userController.changePassword);
router.get('/', userController.getUser);
router.delete('/', userController.deleteUser);

module.exports = router;
