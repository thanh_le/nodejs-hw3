/* eslint-disable no-undef */
const express = require('express');
const router = new express.Router();
const authController = require('../controllers/AuthController');

router.post('/register', authController.register);
router.post('/login', authController.login);
router.post('/forgot_password', authController.resetPassword);

module.exports = router;
