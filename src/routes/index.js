/* eslint-disable no-undef */
const authRoutes = require('./authRoutes');
const userRoutes = require('./userRoutes');
const truckRoutes = require('./truckRoutes');
const loadRoutes = require('./loadRoutes');
const { verifyToken, verifyDriver } = require('../middlewares/auth');

function router(app) {
  app.use('/api/users/me', verifyToken, userRoutes);
  app.use('/api/trucks', verifyToken, verifyDriver, truckRoutes);
  app.use('/api/loads', verifyToken, loadRoutes);
  app.use('/api/auth', authRoutes);
}

module.exports = router;
