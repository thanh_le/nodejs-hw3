/* eslint-disable no-undef */
const express = require('express');
const router = new express.Router();
const truckController = require('../controllers/TruckController');

router.get('/:id', truckController.getTruck);
router.post('/:id/assign', truckController.assignTruck);
router.put('/:id', truckController.updateTruck);
router.delete('/:id', truckController.deleteTruck);
router.get('/', truckController.getTruckList);
router.post('/', truckController.addTruck);

module.exports = router;
