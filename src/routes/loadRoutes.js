/* eslint-disable no-undef */
const express = require('express');
const router = new express.Router();
const { verifyDriver, verifyShipper } = require('../middlewares/auth');
const loadController = require('../controllers/LoadController');

router.get(
  '/:id/shipping_info',
  verifyShipper,
  loadController.getShippingInfor
);
router.get('/:id', loadController.getLoad);
router.post('/:id/post', verifyShipper, loadController.postLoad);
router.put('/:id', verifyShipper, loadController.updateLoad);
router.delete('/:id', verifyShipper, loadController.deleteLoad);
router.patch('/active/state', verifyDriver, loadController.iterateNextState);
router.get('/active', verifyDriver, loadController.getActiveLoadList);
router.get('/', loadController.getLoadList);
router.post('/', verifyShipper, loadController.addLoad);

module.exports = router;
